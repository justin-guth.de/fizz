<?php

namespace Database\Seeders;

use App\Models\LinkPurchaseDrinks;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Purchase;

class PurchasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPurchase("xxX_rupp_Xxx@example.com", ["cola" => 3]);
        $this->createPurchase("xxX_rupp_Xxx@example.com", ["fanta" => 1, "cola" => 1]);
        $this->createPurchase("max.mustermann@example.com", ["paulaner spezi" => 5]);
        $this->createPurchase("Worschi@example.com", ["fanta" => 1, "cola" => 1]);
        $this->createPurchase("Worschi@example.com", ["paulaner spezi" => 5]);
    }

    private function drinkId(string $name)
    {
        return DB::table('drinks')->where("name", $name)->get()->first()->id;
    }

    private function accountId(string $email)
    {
        return DB::table('accounts')->where("email", $email)->get()->first()->id;
    }

    private function createPurchase(string $email, array $drinkNames)
    {
        $purchase = Purchase::create(
            [
                "account_id" => $this->accountId($email),
            ]
        );
        foreach ($drinkNames as $name => $count) {
            LinkPurchaseDrinks::create(
                [
                "purchase_id" => $purchase->id,
                "drink_id" => $this->drinkId($name),
                "count" => $count
            ]
            );
        }
    }
}
