<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Drink;

class DrinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Drink::create(

            [
                "name" => "cola",
                "cost" => 80,
                "stock" => 53
            ]
        );

        Drink::create(

            [
                "name" => "fanta",
                "cost" => 90,
                "stock" => 13
            ]
        );

        Drink::create(

            [
                "name" => "club-mate",
                "cost" => 70,
                "stock" => 20
            ]
        );

        Drink::create(

            [
                "name" => "paulaner spezi",
                "cost" => 100,
                "stock" => 15
            ]
        );

        Drink::create(

            [
                "name" => "квас",
                "cost" => 150,
                "stock" => 10
            ]
        );

        Drink::create(

            [
                "name" => "🍆💦",
                "cost" => 9999,
                "stock" => 1,
            ]
        );
    }
}
