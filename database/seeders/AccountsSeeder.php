<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Account;
use Illuminate\Support\Facades\Hash;

class AccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create(

            [

                "email" => "max.mustermann@example.com",
                "firstname" => "Max",
                "lastname" => "Mustermann",
                "drink_credit_balance" => -450,
                "password_hash" => Hash::make("ligma"),

            ]
        );

        Account::create(
            [

                "email" => "Worschi@example.com",
                "firstname" => "Thomas",
                "lastname" => "Worsch",
                "drink_credit_balance" => -40,
                "password_hash" => Hash::make("ligma"),

            ]
        );

        Account::create(

            [

                "email" => "xxX_rupp_Xxx@example.com",
                "firstname" => "Peter",
                "lastname" => "Rupp",
                "drink_credit_balance" => -4500,
                "password_hash" => Hash::make("ligma"),

            ]
        );
    }
}
