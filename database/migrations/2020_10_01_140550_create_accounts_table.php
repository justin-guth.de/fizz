<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("email", 191)->unique();
            $table->string("firstname", 64);
            $table->string("lastname", 64);
            $table->timestamp("email_verified_at")->nullable()->default(null);
            $table->binary('password_hash');
            //TODO: password hash
            //TODO: remember token
            $table->integer("drink_credit_balance")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
