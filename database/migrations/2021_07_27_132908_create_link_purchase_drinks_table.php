<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkPurchaseDrinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_purchase_drinks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('drink_id')->references('id')->on('drinks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignUuid('purchase_id')->references('id')->on('purchases')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger("count")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_purchase_drinks');
    }
}
