<?php

namespace App\Http\Controllers;

use App\Models\LinkPurchaseDrinks;
use App\Models\Purchase;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends Controller
{
    public function createPurchase(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make(
            $data,
            [
                'drinks'    => 'required|array|min:1',
                'drinks.*'  => 'required|array|distinct|size:2',
                'drinks.*.id'  => 'required|uuid|distinct|exists:drinks,id',
                'drinks.*.amount'  => 'required|integer|min:1',
            ]
        );
        if ($validator->fails()) {
            HTTPResponse::abortBadRequest("The provided data was invalid!", ["failed" => $validator->failed()]);
        }
        $account = Auth::user();
        $purchase = Purchase::create(['account_id' => $account->id]);
        $resultingLinks = [];
        foreach($data['drinks'] as $link)
        {
            $resultingLinks[] = LinkPurchaseDrinks::create([
                'drink_id' => $link['id'],
                'purchase_id' => $purchase->id,
                'count' => $link['amount'],
            ]);
        }

        return HTTPResponse::success($resultingLinks);
    }

    public function getPurchases()
    {
        $account = Auth::user();
        return HTTPResponse::success($account->purchases->toArray());
    }

    public function getPurchaseDetails(string $id)
    {
        $purchaseDrinks = LinkPurchaseDrinks::with("drink")->wherePurchaseId($id)->get();
        return HTTPResponse::success($purchaseDrinks->toArray());
    }
}
