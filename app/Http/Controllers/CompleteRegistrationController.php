<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CompleteRegistrationController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data data passed by the request
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data,
            [
                'email' => [
                    'required',
                    'email',
                    'exists:accounts,email',
                ],
                'token' => ['required', 'exists:accounts,password_hash'],
                'password' => ['required', 'string', 'min:4', 'confirmed'],
            ],
        );
    }


    /**
     * Handle a verification request for the application.
     *
     * @group Authentication
     * @bodyParam activation_code string required the activation code
     * @bodyParam schoolclass string required the ID of the school class
     * @bodyParam token string required the token send per mail
     * @bodyParam password string required
     * @bodyParam password_confirmation string required
     *
     * @param Request $request current request
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function verify(Request $request)
    {
        $data = $request->all();

        $this->validator($data)->validate();

        $account = Account::where('email', $data['email'])->firstOrFail();

        if ($account->password_hash != $data["token"]) {
            HTTPResponse::abortBadRequest("Provided token does not match user account.");
        }

        $account->password_hash = Hash::make($data['password']);

        $account->save();

        // Check if necessary @Justin @Leonard
        // event(new Registered($account));

        return HTTPResponse::success();
    }

}
