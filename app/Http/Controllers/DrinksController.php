<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Drink;
use App\Utility\HTTPResponse;

class DrinksController extends Controller
{
    public function getDrinkInformation(string $drinkId)
    {
        $drink = Drink::findOrFail($drinkId);

        return HTTPResponse::success($drink->toArray());
    }

    public function getDrinkList()
    {
        $drinks = Drink::all();

        return HTTPResponse::success($drinks->toArray());
    }

}
