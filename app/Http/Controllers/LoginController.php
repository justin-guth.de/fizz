<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Utility\HTTPResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function createToken(Request $request)
    {
        $data = $request->all();

        Validator::make(
            $data,
            [
                'email' => ['required', 'email'],
                'password' => ['required', 'string'],
                'device_name' => ['required', 'string'],
                'remember' => ['boolean'],
            ]
        )->validate();

        $user = Account::where("email", $data['email'])->firstOrFail();
        $pwhash = $user->password_hash;


        if (!Hash::check($request->password, $pwhash)) {
            return HTTPResponse::badRequest('The provided credentials are incorrect.');
        }

        return HTTPResponse::success(["token" => $user->createToken($request->device_name)->plainTextToken]);
    }
}
