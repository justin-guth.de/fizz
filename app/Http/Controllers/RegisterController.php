<?php

/**
 * @project Prima Tandems
 * @author Adrian Keller
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\AccountValidationMail;
use App\Models\Account;
use App\Models\ActivationCode;
use App\Providers\RouteServiceProvider;
use App\Rules\ActivationCodeUnused;
use App\Utility\HTTPResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Controller which handles registration
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data data passed by the request
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data,
            [
                'firstname' => ['required', 'string', 'max:64'],
                'lastname' => ['required', 'string', 'max:64'],
                'email' => ['required', 'string', 'email', 'max:191', 'unique:accounts', 'confirmed'],
                //'agreement_agb' => ['accepted'],
                //'agreement_ds' => ['accepted'],
            ]
        );
    }


    /**
     * Handle a registration request for the application.
     *
     * To complete the registration a link provided per Mail must be clicked. This redirects to the validation endpoint.
     *
     * @group     Authentication
     * @bodyParam firstname string required firstname
     * @bodyParam lastname string required Lastname
     * @bodyParam activation_code string required Activation Code
     * @bodyParam email string required E-Mail
     * @bodyParam agreement_agb boolean required agreement to terms of service
     * @bodyParam agreement_ds boolean required agreement to data protection policy
     * @bodyParam agreement_newsletter boolean agreement to newsletter
     *
     * @param Request $request current request
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();


        $response = $this->create($request->all());

        return $response;
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data information about new user
     *
     * @return Account
     */
    protected function create(array $data)
    {
        $token = mt_rand(0, 999999);

        $account = Account::create(
            [
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'password_hash' => $token,
            ]
        );

        Mail::to($data['email'])->send(
            new AccountValidationMail($data['email'], $token)
        );

        return HTTPResponse::created(["account_id"=> $account->id]);
    }


}
