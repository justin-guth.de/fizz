<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountValidationMail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @param string $activationCode activation code linked to this account
     * @param string $token          token to validate mail
     *
     * @return void
     */
    public function __construct(string $email, string $token)
    {
        $this->email = $email;
        $this->token = $token;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[Fizz] Bitte bestätige Deine E-Mail-Adresse")
            ->bcc(env('MAIL_BCC_ADDRESS', 'fizz@justin-guth.de'), "Fizz")
            ->markdown('emails.auth.accountValidationMail')
            ->with([
                'token'          => $this->token,
                'email'          => $this->email,
                "url"            => env('WEB_URL') . "/complete_registration/" . $this->email . "/" . $this->token,
            ]);
    }
}
