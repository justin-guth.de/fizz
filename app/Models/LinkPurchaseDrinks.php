<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinkPurchaseDrinks extends Model
{
    use HasFactory;
    use Uuid;

    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'link_purchase_drinks';

    protected $fillable = [
        'drink_id',
        'purchase_id',
        'count',
    ];

    public function drink()
    {
        return $this->belongsTo(Drink::class);
    }
}
