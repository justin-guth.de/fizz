<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Drink extends Model
{
    use HasFactory;
    use Uuid;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'name',
        'cost',
        'stock',
    ];

    public function purchases()
    {
        return $this->hasMany("App\Models\Purchase");
    }

    public function toArray()
    {
        return array(
            "id" => $this->id,
            "name" => $this->name,
            "cost" => $this->cost,
            "stock" => $this->stock,
        );
    }

}
