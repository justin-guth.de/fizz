<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Utility\EnhancedTestCase;
use App\Models\Drink;

use function PHPUnit\Framework\assertCount;

class DrinkRetrievalTest extends EnhancedTestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGettingDrinkList()
    {
        $response = $this->get('/api/drinks');

        $response->assertStatus(200);

        assertCount(6,$response->getOriginalContent());
    }

    public function testGettingDrinkInformation()
    {
        $drinkId = Drink::where('name', 'квас')->first()->id;

        $response = $this->get('/api/drink/' . $drinkId);

        $response->assertStatus(200);

        $response->assertExactJson(
            [
                'name' => 'квас',
                'id' => $drinkId,
                'stock' => 10,
                'cost' => 150,
            ]
        );
    }

    public function testGettingDrinkInformationWithWrongId()
    {
        $drinkId = '00000000-0000-0000-0000-000000000000';

        $response = $this->get('/api/drink/' . $drinkId);

        $response->assertStatus(404);
    }
}
