<?php

namespace Tests\Utility;


use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class EnhancedTestCase extends TestCase
{
    private $user;
    private static bool $isSetup = false;
    private static string $databaseBuildString = "";

    public function setUp(): void
    {
        parent::setUp();

        if (!EnhancedTestCase::$isSetup)
        {
            $this->resetSeed();
            $this->dumpDatabase();

            EnhancedTestCase::$databaseBuildString = file_get_contents(database_path() . '/backups/test_db/db.sql');
            printf("Setting up database seed... Dumping Database...");
            EnhancedTestCase::$isSetup = true;
        }

        $this->loadFromDump();
        //$this->resetSeed();
    }


    protected function resetDatabase()
    {
        Artisan::call('migrate:fresh');
    }

    protected function seedDatabase()
    {
        Artisan::call('db:seed', ['--class' => 'DatabaseSeeder']);
    }

    protected function resetSeed()
    {
        $this->resetDatabase();
        $this->seedDatabase();

        // TODO read pregenerated seeded db into mysql
    }

    protected function loadFromDump()
    {

        $content =EnhancedTestCase::$databaseBuildString;
        DB::unprepared($content);
    }

    protected function dumpDatabase()
    {
        Artisan::call('db:dumpt');
    }
}
