<?php

use App\Http\Controllers\CompleteRegistrationController;
use App\Http\Controllers\DrinksController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/drink/{id}",[DrinksController::class, "getDrinkInformation"]);

Route::get('/drinks',[DrinksController::class, "getDrinkList"]);

Route::post('/tokens/create', [LoginController::class, "createToken"]);
Route::post('/register', [RegisterController::class, 'register']);
Route::patch('/verify', [CompleteRegistrationController::class, 'verify']);
