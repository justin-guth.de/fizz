@component('mail::message')
# Hallo,

Fast geschafft, bestätige bitte deine E-Mail Adresse.

@component("mail::button", ["url" => $url])
    E-Mail-Adresse bestätigen
@endcomponent

Falls der Button nicht funktioniert probier es unter dem link:
[{{$url}}]({{$url}})

Liebe Grüße,
Dein Team von Fizz
@endcomponent
